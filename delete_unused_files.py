import sys
import json
import os
import io

assets_dir = os.path.join(os.path.dirname(__file__),'assets')
exclude_assets_files = ['actionButtonBack.png','actionIconSelect.png','Shop-48.png','cashBar.png','clothingLine.png','gold_menu_small.png','coinsBar.png','construct.mp3','dirtBackground.png','energyBar.png','fenceSW.png','flowerPot1.png','friendTile.png','giftButton.png','greenBackground.png','greendot.png','green+.png','leftButton1.png','leftButton2.png','leftButton3.png','mainMenuBackground.png','marketItemBack.png','marketMenuBack.png','dollar_menu_small.png','whats_new_icon.png','reddot.png','rightButton1.png','rightButton2.png','rightButton3.png','wallSE.png','xpBar.png','aharoni_26px.png','arrow.js','cash.png','Banknotes.png','close.png','coin.png','Coin1.png','dialogOpen.mp3','Expand_2.png','rort.png','GameLose1.wav','GameWin1Fast.wav','Shop_2.png','creatur_footstep_large.mp3','generalclick04.wav','outline.js','press_start_2p_26pt.png','rectangle.js','select.mp3','Sickle-48.png','star.png','actionIconSelect.png','valleyBackground.png','villagedash_24px.png','villager.png','Water-48.png','WateringCan-48.png','Welcome.wav','xbutton.png','Level1Full.wav','Welcome.wav','hut2.png','plantsSmall.png','hut1.png','well2.png','woodSmall.png','woodLarge.png','well.png','well3.png','computerRoom.png','sewingRoom.png','shop.png','generator.png','drilling_truck.png','smokeyHut.png','farm03.png','huticon.png','yerticon.png','pitcherwellicon.png','woodSmall.png','woodLarge.png','biosandfiltericon.png','borewellicon.png','computerRoomicon.png','sewingRoomicon.png','shopicon.png','generatoricon.png','drilltruckicon.png','smokeyHut.png','No_Image_Available_75.png','click.mp3','creaturfootsteplarge.mp3','cancel.mp3','creditCardFillSample.png']
assets_files = [os.path.join(root, name) for root, dirs, files in os.walk(assets_dir) for name in files if name.endswith((".png", ".jpg", ".jpeg", ".webp", ".mp3", ".ogg")) and name not in exclude_assets_files]
client_dir = os.path.join(os.path.dirname(__file__),'client')
exclude_client_files = ['game.js','every.js','gameStart.min.js','crypto-js-hmac.js']
client_files = [os.path.join(root, name) for root, dirs, files in os.walk(client_dir) for name in files if name.endswith((".js", ".html", ".htm", ".css")) and name not in exclude_client_files]

found_dict = {}
for fn in client_files:
    with io.open(os.path.join(os.path.dirname(__file__), fn), 'r', encoding='utf-8') as file:
        for line in file:
            for asset in assets_files:
                head, tail = os.path.split(asset)
                found_dict[tail] = 'not found'
                if tail in line:
                    found_dict[tail] = file.name
                    assets_files.remove(asset)

for asset in assets_files:
    os.remove(asset)

print found_dict